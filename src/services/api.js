import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:9080/api/smart-stock-ia-service',
});

export default api;