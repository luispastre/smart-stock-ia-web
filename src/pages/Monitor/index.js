import React, {useEffect, useState } from 'react';

import Chart from 'react-google-charts';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import './styles.css';
import Modal from "react-modal";


export default function Monitor(){

    const [isOpen, setIsOpen] = useState(false);
    const [stocks, setStocks] = useState([]);
    const [cotation, setCotation] = useState(0.0);
    const [stockId, setStockId] = useState(1);
    const [resultPETR4, setResultPETR4] = useState('');
    const [resultSQIA3, setResultSQIA3] = useState('');
    const [resultVALE3, setResultVALE3] = useState('');
    const [resultVISC11, setResultVISC11] = useState('');
    const [resultHGLG11, setResultHGLG11] = useState('');


    function toggleModal() {
        setIsOpen(!isOpen);
    }

    function onChangeUser(event) {
       setStockId(event.target.value);
    }

    async function doSimulate(event){
        event.preventDefault();
        let response = await api.post('/data/simulatePrice', {price:cotation , stockId});
        setIsOpen(!isOpen);  

        console.log(response.data);
        if(stockId == 1)
            setResultPETR4(response.data);
        if(stockId == 2)
            setResultVALE3(response.data);
        if(stockId == 3)
            setResultVISC11(response.data);
        if(stockId == 4)
            setResultHGLG11(response.data);
        if(stockId == 5)
            setResultSQIA3(response.data);
    }


    const [optionsSQIA3, setOptionsSQIA3] = useState({
        hAxis: {
          title: 'Data',
        },
        vAxis: {
           title: 'Preço',
           format: 'currency',
           scaleType: 'log',
           gridlines: {
               minSpacing: 3
           }
        },
        chartArea: { bottom:80 , width: '90%', height: '80%' },
        legend: { position: 'top' },
        title: 'SQIA3',
        pointSize: 5,
        height: 600,

    });
    const [optionsPETR4, setOptionsPETR4] = useState({
        hAxis: {
          title: 'Data',
        },
        vAxis: {
           title: 'Preço',
           format: 'currency',
           scaleType: 'log',
           gridlines: {
               minSpacing: 3
           }
        },
        chartArea: { bottom:80 , width: '90%', height: '80%' },
        legend: { position: 'top' },
        title: 'PETR4',
        pointSize: 5,
        height: 600
    });
    const [optionsVALE3, setOptionsVALE3] = useState({
        hAxis: {
          title: 'Data',
        },
        vAxis: {
           title: 'Preço',
           format: 'currency',
           scaleType: 'log',
           gridlines: {
                minSpacing: 3
           }
        },
        chartArea: { bottom:80 , width: '90%', height: '80%' },
        legend: { position: 'top' },
        title: 'VALE3',
        pointSize: 5,
        height: 600
    });
    const [optionsVISC11, setOptionsVISC11] = useState({
        hAxis: {
          title: 'Data',
        },
        vAxis: {
           title: 'Preço',
           format: 'currency',
           scaleType: 'log',
           gridlines: {
                minSpacing: 3
           }
        },
        chartArea: { bottom:80 , width: '90%', height: '80%' },
        legend: { position: 'top' },
        title: 'VISC11',
        pointSize: 5,
        height: 600
    });
    const [optionsHGLG11, setOptionsHGLG11] = useState({
        hAxis: {
          title: 'Data',
        },
        vAxis: {
           title: 'Preço',
           format: 'currency',
           scaleType: 'log',
           gridlines: {
                minSpacing: 3
           }
        },
        chartArea: { bottom:80 , width: '90%', height: '80%' },
        legend: { position: 'top' },
        title: 'HGLG11',
        pointSize: 5,
        height: 600
    });


    const [dataSQIA3, setDataSQIA3] = useState([]);
    const [dataPETR4, setDataPETR4] = useState([]);
    const [dataVALE3, setDataVALE3] = useState([]);
    const [dataVISC11, setDataVISC11] = useState([]);
    const [dataHGLG11, setDataHGLG11] = useState([]);
    
    Modal.setAppElement("#root");

    async function loadMonitorSQIA3 (qtRows){

        const response = await api.get('/data/' + 'SQIA3' + '/' + qtRows);
        
        const ldata = [];
        const header = ['Data', 'Real', 'Predito'];

        ldata[0] = header; 
        let index = 1; 
        response.data.map(item => {
            const row = [item.date, item.realPrice, item.predictPrice];
            ldata[index] = row;
            index ++;
        });

       setDataSQIA3(ldata);
    }
    async function loadMonitorPETR4 (qtRows){
        const response = await api.get('/data/' + 'PETR4' + '/' + qtRows);
        
        const ldata = [];
        const header = ['Data', 'Real', 'Predito'];

        ldata[0] = header; 
        let index = 1; 
        response.data.map(item => {
            const row = [item.date, item.realPrice, item.predictPrice];
            ldata[index] = row;
            index ++;
        });

       setDataPETR4(ldata);
    }
    async function loadMonitorVALE3 (qtRows){
        const response = await api.get('/data/' + 'VALE3' + '/' + qtRows);
        
        const ldata = [];
        const header = ['Data', 'Real', 'Predito'];

        ldata[0] = header; 
        let index = 1; 
        response.data.map(item => {
            const row = [item.date, item.realPrice, item.predictPrice];
            ldata[index] = row;
            index ++;
        });

       setDataVALE3(ldata);
    }
    async function loadMonitorVISC11 (qtRows){
        const response = await api.get('/data/' + 'VISC11' + '/' + qtRows);
        
        const ldata = [];
        const header = ['Data', 'Real', 'Predito'];

        ldata[0] = header; 
        let index = 1; 
        response.data.map(item => {
            const row = [item.date, item.realPrice, item.predictPrice];
            ldata[index] = row;
            index ++;
        });

       setDataVISC11(ldata);
    }
    async function loadMonitorHGLG11 (qtRows){
        const response = await api.get('/data/' + 'HGLG11' + '/' + qtRows);
        
        const ldata = [];
        const header = ['Data', 'Real', 'Predito'];

        ldata[0] = header; 
        let index = 1; 
        response.data.map(item => {
            const row = [item.date, item.realPrice, item.predictPrice];
            ldata[index] = row;
            index ++;
        });

       setDataHGLG11(ldata);
    }

    async function findAllStock(){
        const response = await api.get('/data/listStocks');
        setStocks(response.data);
    }

    useEffect(() => {

        loadMonitorSQIA3(5);
        loadMonitorPETR4(5);
        loadMonitorVALE3(5);
        loadMonitorVISC11(5);
        loadMonitorHGLG11(5);
        findAllStock();

    },[]);


    function onChangeQtde(event) {
        loadMonitorSQIA3(event.target.value);
        loadMonitorPETR4(event.target.value);
        loadMonitorVALE3(event.target.value);
        loadMonitorVISC11(event.target.value);
        loadMonitorHGLG11(event.target.value);
     }

    return (
        <>
        <div className="divRegister">
            <button onClick={toggleModal}>Simular</button>
            <label htmlFor="qtRegister">Período:</label>
            <select id="qtRegister" onChange={onChangeQtde}>
                <option key="1" value="5">1 semana</option>
                <option key="2" value="10">2 semanas</option>
                <option key="3" value="30">1 mês</option>
                <option key="4" value="60">2 meses</option>
            </select>
        </div>
        <div>
            <Modal
                isOpen={isOpen}
                onRequestClose={toggleModal}
                contentLabel="My dialog"
                className="mymodal"
                overlayClassName="myoverlay">
                <div>
                <p>
                    <strong>Simular Cotação de Ação</strong>
                </p>
                </div>
                <form >
                    <label htmlFor="dropdown">AÇÃO *</label>

                    <select id="dropdown"  onChange={onChangeUser}>
                        {stocks.map((stock, index) => {
                            return <option key={stock.id} value={stock.id}>{stock.symbol}</option>
                        })}
                    </select>

                    <label htmlFor="valor">VALOR *</label>
                    <input   
                        type="valor" 
                        id="valor" 
                        value={cotation}
                        onChange={event => setCotation(event.target.value)}
                        placeholder="Seu valor" />
                    <button className="btn" onClick={doSimulate}>Simular</button>
                </form>
            </Modal>
        </div>

        
        <div className="App">
            <header className="App-header">
            <div className="divResult"><span className="myspan">{resultSQIA3}</span></div>
                <div>
                    <Chart
                        width={'100%'}
                        height={'100%'}
                        chartType="LineChart"
                        data={dataSQIA3}
                        options={optionsSQIA3}
                    />
                </div>

                <div className="divResult"><span className="myspan">{resultPETR4}</span></div>
                <div>
                    <Chart
                        width={'100%'}
                        height={'100%'}
                        chartType="LineChart"
                        data={dataPETR4}
                        options={optionsPETR4}
                    />
                </div>
                <div className="divResult"><span className="myspan">{resultVALE3}</span></div>
                <div>
                    <Chart
                        width={'100%'}
                        height={'100%'}
                        chartType="LineChart"
                        data={dataVALE3}
                        options={optionsVALE3}
                    />
                </div>
                <div className="divResult"><span className="myspan">{resultVISC11}</span></div>
                <div>
                    <Chart
                        width={'100%'}
                        height={'100%'}
                        chartType="LineChart"
                        data={dataVISC11}
                        options={optionsVISC11}
                    />
                </div>
                <div className="divResult"><span className="myspan">{resultHGLG11}</span></div>
                <div>
                    <Chart
                        width={'100%'}
                        height={'100%'}
                        chartType="LineChart"
                        data={dataHGLG11}
                        options={optionsHGLG11}
                    />
                </div>
            </header>
        </div>
        <div>
            <Link to="/history">
                    <button className="btn">Ir para Importação de Histórico</button>
            </Link>
        </div> 
        </>
    );

}