import React, { useState } from 'react';
import api from '../../services/api';

export default function Login({ history }){
   
    const [email, setEmail] = useState('');

    async function handleSubmit(event){
       event.preventDefault();
       
       const response = await api.post('/test/sessao', { email });
             
       const { id: id } = response.data;
       
       console.log(id);
       localStorage.setItem('user', id);
       
       history.push('/dashboard');
     }

    return (
        <>
          <p>
            <strong>Smart-Stock Ready</strong> boga
          </p>
          <form onSubmit={handleSubmit}>
            <label htmlFor="email">E_MAIL *</label>
            <input 
                type="email" 
                id="email" 
                value={email}
                onChange={event => setEmail(event.target.value)}
                placeholder="Seu melhor e-mail" />

            <button className="btn" type="submit">Entrar</button>
          </form>
        </>
    );
    
}