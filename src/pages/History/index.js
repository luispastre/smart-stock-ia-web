import React, { useState, useMemo } from 'react';
import camera from '../../assets/robo-imagem-animada-0049.gif';
import api from '../../services/api';
import './styles.css';
import { Link } from 'react-router-dom';

export default function History({ history }){

    const [thumbnail, setThumbnail] = useState(false);

    const [ibagem, setIbagem] = useState(null);

    async function handleSubmit(event){
        event.preventDefault();
        setIbagem(camera);
        const data = new FormData();
        data.append('file', event.target.files[0]);
        await api.post('/data/importHistory', data);
        setIbagem(null);
         history.push('/');
    }


    return (
        <form>
            <label id="thumbnail" className={ibagem ? 'no-has-thumbnail' : 'has-thumbnail'}><span>SELECIONE O ARQUIVO</span>
                <input type="file"  onChange={event => handleSubmit(event)}/>
                <img src={ibagem} alt="Select file"/>
            </label>
            <Link to="/">
                <button className="btn">ir para Monitor</button>
            </Link>
        </form>
    )

}